/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encriptacion;

/**
 *
 * @author rozo-
 */
public class Encriptacion {

    int modulo = 27;
    char abecedario[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    int equivalente[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26};
    String palabra = "";
    int matriz[][] = {{1, 2, 3}, {0, 4, 5}, {1, 0, 6}};
// matriz para dfesencriptar       int matriz[][] = {{6, 24, 22 }, {26, 21, 1}, {17, 5, 10}};

    int x, y = 0;

    public Encriptacion(String palabra) {
        this.palabra = palabra;
    }

    public char[][] terna() {
        int contadorInterno = 0;
        int contadorExterno = 0;
        char[] palabraChar = palabra.toCharArray();
        x = (palabraChar.length/3)+1;
        y = 3;

        int rta[][] = new int[x][y];

        for (int i = 0; i < palabraChar.length; i++) {
            for (int j = 0; j < abecedario.length; j++) {
                if (palabraChar[i] == abecedario[j]) {
                    if (contadorExterno == 3) {
                        contadorExterno = 0;
                        contadorInterno++;
                    }

                    rta[contadorInterno][contadorExterno] = equivalente[j];
                    contadorExterno++;
                }
            }
        }
        return palabraConvertida(modulo(multiplicacion(rta)));

    }
    
    public int[][] multiplicacion(int rta[][]) {
        int rtaa[][] = new int[x][y];
        for (int i = 0; i < rta.length; i++) {

            rtaa[i] = multiMatriz(rta[i][0], rta[i][1], rta[i][2]);

        }

        return rtaa;
    }

    public int[] multiMatriz(int numero1, int numero2, int numero3) {
        int rtaa[] = new int[y];
        for (int i = 0; i < matriz.length; i++) {
            rtaa[i] = (matriz[i][0] * numero1) + (matriz[i][1] * numero2) + (matriz[i][2] * numero3);
        }

        return rtaa;
    }

    public int[][] modulo(int rta[][]) {
        int rtaa[][] = new int[x][y];
        for (int i = 0; i < rta.length; i++) {
            for (int j = 0; j < rta[i].length; j++) {
                rtaa[i][j] = rta[i][j];
                do {
                    if (rtaa[i][j] >= 27) {
                        rtaa[i][j] = rtaa[i][j] - 27;
                    } else {
                        rtaa[i][j] = rta[i][j];
                    }

                } while (rtaa[i][j] >= 27);

            }
        }

        return rtaa;
    }

    public char[][] palabraConvertida(int mod[][]) {
        char[][] rtaa = new char[x][y];

        for (int i = 0; i < mod.length; i++) {
            for (int j = 0; j < mod[i].length; j++) {
                for (int k = 0; k < equivalente.length; k++) {
                    if (mod[i][j] == equivalente[k]) {
                        rtaa[i][j] = abecedario[k];

                    }
                }
            }
        }

        return rtaa;
    }
}
